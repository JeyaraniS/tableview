import React from 'react'
import TableView from './TableView'

const Dashboard = () => {
    return (
        <div className="dashboard">
            <TableView/>
        </div>
    )
}

export default Dashboard
