import axios from "axios";
import { FETCH_ITEM, FETCH_ITEM_FAILS, FETCH_ITEM_SUCCESS } from "./TableViewType";

const BASE_URL = 'https://jsonplaceholder.typicode.com/posts';

const getItem = () => (dispatch) => {
    dispatch({ type: FETCH_ITEM });
  
    axios.get(`${BASE_URL}`)
      .then((res) => {
        // console.log(res.data)
        dispatch({
          type: FETCH_ITEM_SUCCESS,
          payload: res.data,
        });
      })
      .catch((error) => {
        dispatch({
          type: FETCH_ITEM_FAILS,
          payload: error,
        });
      });
  };

  export{
    getItem
  }