import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { getItem } from './TableViewAction';

const TableView = () => {
    const dispatch = useDispatch();
    const itemData = useSelector((state) => state?.item?.itemData?.data || []);

    const [currentPage, setCurrentPage] = useState(1);
    const [dataLimit, setDataLimit] = useState(5);

    useEffect(() => {
        dispatch(getItem());
    }, []);

    const count = itemData.length
    const pageLimit = 5
    const totalpages=count/pageLimit

    const start=currentPage*pageLimit-5
    const showData=itemData.slice(start, (start+parseInt(dataLimit)))

    const nextPage = () => {
        setCurrentPage((page) => page + 1);
    }

    const previousPage = () => {
        setCurrentPage((page) => page - 1);
    }

    const changePage = (event) => {
        const pageNumber = Number(event.target.textContent);
        setCurrentPage(pageNumber);
    }

    const onchangeLimit = (e) => {
        setDataLimit(e.target.value)
    }

    const getPaginationGroup = () => {
        let start = Math.floor((currentPage - 1) / pageLimit) * pageLimit;
        return new Array(pageLimit).fill().map((_, idx) => start + idx + 1);
    };

    return (
        <div>
            <div className="div-pad ">
                <span className="count bold ">{count} Total Posts</span >
                <span className="border-vert"></span >
                <span className="count bold">Page {currentPage}/{totalpages}</span >
            </div>
            <table className="table-view">
                <thead>
                    <tr className="table-border">
                        <td className="table-head bold ">#</td>
                        <td className="table-col1 bold">Title</td>
                        <td className="table-col2 bold">Body</td>
                    </tr>
                </thead>
                <tbody>
                    {showData?.map(item =>
                        <tr key={item.id}>
                            <td className="table-head bold">{item.id}</td>
                            <td className="table-col1">{item.title}</td>
                            <td className="table-col2">{item.body}</td>
                        </tr>)}
                </tbody>
            </table>
            <div className="div-pad ">
                <input type="number" placeholder="5" min="5" max={count} name="limit" onChange={(e) => onchangeLimit(e)} />
                <button  onClick={previousPage} disabled={currentPage<2?true:false}>prev</button>
                {getPaginationGroup().map((item, index) => (
                    <button key={index} onClick={changePage}><span>{item}</span></button>
                ))}
                <button onClick={nextPage} disabled={currentPage>=totalpages?true:false}>next</button>
            </div>
        </div>
    )
}

export default TableView
