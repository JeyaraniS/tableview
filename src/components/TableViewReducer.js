import { FETCH_ITEM, FETCH_ITEM_FAILS, FETCH_ITEM_SUCCESS } from "./TableViewType";

const TableViewReducer = (
  state = {
    itemData: {
      fetching: false,
      data: [],
      error: { status: false, message: null },
    },
  }, action,
) => {
  switch (action.type) {
    case FETCH_ITEM:
      return {
        ...state,
        itemData: {
          fetching: true,
          error: { status: false, message: null },
          data: [],
        },
      };

    case FETCH_ITEM_SUCCESS:
      return {
        ...state,
        itemData: {
          ...state.itemData,
          fetching: false,
          data: action.payload,
        },
      };
    case FETCH_ITEM_FAILS:
      return {
        ...state,
        itemData: {
          fetching: false,
          error: { status: true, message: action.payload },
          data: [],
        },
      };

    default:
      return state;
  }
}
export { TableViewReducer };