import { TableViewReducer } from '../components/TableViewReducer';

const { combineReducers } = require('redux');

export const rootReducer = combineReducers({
  item: TableViewReducer,
});

export default rootReducer;