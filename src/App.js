import { Route, Switch, BrowserRouter as Router } from 'react-router-dom';
import './assets/css/tableView.css';
import './assets/css/dashboard.css';
import Dashboard from './components/Dashboard';

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={Dashboard} />
        {/* <Route path="/summary" component={Summary} /> */}
      </Switch>
    </Router>
  );
}

export default App;
